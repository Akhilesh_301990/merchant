﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedbank.Web.API.Merchant.DTO.Application
{
   public class BusinessType
    {
        public int businessTypeId { get; set; }
        public string businessTypeName { get; set; }
    }
}
