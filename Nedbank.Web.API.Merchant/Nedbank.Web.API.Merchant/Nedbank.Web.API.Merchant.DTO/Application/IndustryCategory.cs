﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class IndustyCategory
    {
        public int industryCategoryId { get; set; }
        public string industryCategoryName { get; set; }
    }
}
