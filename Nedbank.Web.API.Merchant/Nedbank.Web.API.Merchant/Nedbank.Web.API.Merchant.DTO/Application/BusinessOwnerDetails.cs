﻿using System;

namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class BusinessOwnerDetails : IDisposable
    {
        public string title { get; set; }
        public string firstName { get; set; }
        public string surname { get; set; }
        public string primaryContactNumber { get; set; }
        public string email { get; set; }

        #region Implementing IDisposible
        /// <summary>
        /// Method to Dispose the class
        /// </summary>        
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
