﻿namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class ProductInfo
    {
        public int productId { get; set; }
        public string productCode{ get; set; }
        public string productName{ get; set; }
        public string productDescription{ get; set; }
        public int price{ get; set; }
        public string priceDescription { get; set; }
    }
}
