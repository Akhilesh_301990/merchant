﻿namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class LeadMerchantInfo
    {
        public int merchantLeadId { get; set; }
        public string title { get; set; }
        public string firstname { get; set; }
        public string surname { get; set; }
        public string emailAddress { get; set; }
        public string primaryContactNumber { get; set; }
        public string alternateContactNumber { get; set; }
    }
}
