﻿using System;

namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class ProductCartInfo
    {
        public int productCartId { get; set; }
        public string productCode { get; set; }
        public int merchantApplicationtId { get; set; }
        public int quantity { get; set; }
        public int totalPrice { get; set; }
        public Int64 referenceId { get; set; }
    }
}
