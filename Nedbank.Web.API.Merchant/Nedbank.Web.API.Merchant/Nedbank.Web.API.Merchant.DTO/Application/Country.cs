﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class Country
    {
        public int countryId { get; set; }
        public string countryName { get; set; }
        public string countryCode { get; set; }

    }
}
