﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class IndustrySubCategory
    {
        public int industySubCategoryId { get; set; }
        public string industySubCategoryName { get; set; }
        public int industryCategoryId { get; set; }
    }
}

