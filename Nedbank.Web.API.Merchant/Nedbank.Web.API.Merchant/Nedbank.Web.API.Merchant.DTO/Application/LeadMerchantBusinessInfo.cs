﻿namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class LeadMerchantBusinessInfo
    {
        public int merchantLeadId { get; set; }
        public string businessName { get; set; }
        public int industryCategory { get; set; }
        public int industySubCategory { get; set; }
        public int businessProvience { get; set; }
        public int monthlyTurnover { get; set; }
        public string alternateContact { get; set; }
    }
}
