﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class Citizenship
    {
        public int citizenShipId { get; set; }
        public string citizenShipName { get; set; }
    }
}
