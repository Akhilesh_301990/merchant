﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class BusinessSubCategory
    {
        public int businessSubCategoryId { get; set; }
        public string businessSubCategoryName { get; set; }
        public int businessTypeId { get; set; }
    }
}
