﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class BusinessProvince
    {
        public int provinceId { get; set; }
        public string provinceName { get; set; }
        public string provinceCode { get; set; }

    }
}
