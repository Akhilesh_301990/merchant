﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nedbank.Web.API.DMO.Controllers;
using Nedbank.Web.API.Merchant.DTO.Application;

namespace MerchantOnboarding.Tests
{
    [TestClass]
    public class TestMerchantLeadController
    {
        [TestMethod]
        public void AddMerchantLeadDetails_ShouldAddNewMerchant()
        {
            var controller = new MerchantLeadController();

            LeadMerchantInfo leadMerchantInfo = new LeadMerchantInfo();
            leadMerchantInfo.firstname = "Ganesh";
            leadMerchantInfo.surname = "Gawande";
            leadMerchantInfo.title = "Mr.";
            leadMerchantInfo.emailAddress = "ganesh.gawande@nihilent.com";
            leadMerchantInfo.alternateContactNumber = "9970045972";
            leadMerchantInfo.primaryContactNumber = "9970045972";

            controller.AddMerchantLeadDetails(leadMerchantInfo);
        }
    }
}
