﻿using Nedbank.Web.API.DMO.Models;
using Nedbank.Web.API.DMO.Models.Providers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Nedbank.Web.API.Merchant
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Initialization of the configuration keys only one time
            ConfigurationProvider.Instance.Init();
            DBErrorConfig.Instance.Init();
        }
    }
}
