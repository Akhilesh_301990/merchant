﻿using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.MerchantLead;
using Nedbank.Web.API.Merchant.DTO.Application;
using System.Web.Http;

namespace Nedbank.Web.API.DMO.Controllers
{
    public class MerchantLeadController : ApiController //BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="leadMerchantInfo"></param>
        /// <returns></returns>
        [Route("v1/merchantleads/addmerchantinfo")]
        [HttpPost]
        public IHttpActionResult AddMerchantLeadDetails(LeadMerchantInfo leadMerchantInfo)
        {
            MerchantLeadWrapper merchantLeadWrapper = new MerchantLeadWrapper();

            return this.Ok(merchantLeadWrapper.AddMerchantLeadInformation(leadMerchantInfo));
            //return this.Response(merchantLeadWrapper.AddMerchantLeadInformation(leadMerchantInfo));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="leadMerchantBusinessInfo"></param>
        /// <returns></returns>
        [Route("v1/merchantleads/addmerchantbusinessinfo")]
        [HttpPost]
        public IHttpActionResult AddMerchantLeadBusinessDetails(LeadMerchantBusinessInfo leadMerchantBusinessInfo)
        {
            MerchantLeadWrapper merchantLeadWrapper = new MerchantLeadWrapper();
            return this.Ok(merchantLeadWrapper.AddMerchantLeadBusinessInformation(leadMerchantBusinessInfo));
            //return this.Response(merchantLeadWrapper.AddMerchantLeadBusinessInformation(leadMerchantBusinessInfo));
        }
    }
}
