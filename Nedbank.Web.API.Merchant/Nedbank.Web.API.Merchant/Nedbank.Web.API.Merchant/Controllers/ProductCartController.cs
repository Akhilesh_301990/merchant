﻿using Nedbank.Web.API.DMO.Models.ProductCart;
using Nedbank.Web.API.Merchant.DTO.Application;
using System.Web.Http;

namespace Nedbank.Web.API.DMO.Controllers
{
    public class ProductCartController : ApiController //BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="leadMerchantInfo"></param>
        /// <returns></returns>
        [Route("v1/productscart/Add")]
        [HttpPost]
        public IHttpActionResult AddProductIntoCard(ProductCartInfo productCartInfo)
        {
            ProductCartWrapper productWrapper = new ProductCartWrapper();
            return this.Ok(productWrapper.AddProductIntoCart(productCartInfo));
            //return this.Response(productWrapper.AddProduct(productInfo));
        }
    }
}
