﻿using API.BO.Communications;
using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.DBCommunication;
using Nedbank.Web.API.Merchant.DTO.Application;
using Nedbank.Web.API.Merchant.Models;
using System.Linq;
using System.Web.Http;

namespace Nedbank.Web.API.Merchant.Controllers
{
    public class MerchantController : ApiController//BaseController 
    {
        [Route("v1/Merchant/{Identifier}")]
        [Route("v1/Merchant/{QueryString}/{Identifier}")]
        [Route("v1/Merchant/Validate")]
        public IHttpActionResult Get()
        {
            //MerchantWrapper merchantWrapper = new MerchantWrapper(this.GetAuthUID(), this.GetIdentityClaims(), this.Request.Headers);
            switch (Request.RequestUri.Segments.Last())
            {
                // URI: v1/Merchant/Refresh
                case MerchantCommon.st_GetRefreash:
                    //return this.Ok(merchantWrapper.RefreshMarchantInformation());
                    return this.Ok();

                case MerchantCommon.st_Validate:
                    //merchantWrapper.RefreshMarchantInformation()
                    return this.Ok();

                default:
                    return this.NotFound();
            }
        }

        [Route("v1/Merchant/Request")]
        public IHttpActionResult PostRequest(Request request)
        {
            return this.Ok();
        }

        [Route("v1/Merchant/SaveBusinessDetails")]
        [HttpPost]
        public IHttpActionResult SaveBusinessDetails(BusinessOwnerInformation businessOwnerInformation)
        {
            using (MerchantDBManager merchantDBManager = new MerchantDBManager())
            {
                merchantDBManager.SaveBusinessDetails(businessOwnerInformation);
            }
            return this.Ok("Tested Ok");
        }

        [Route("v1/Merchant/SaveBusinessOwnerDetails")]
        [HttpPost]
        public IHttpActionResult SaveBusinessOwnerDetails(BusinessOwnerDetails businessOwnerDetails)
        {
            using (MerchantDBManager merchantDBManager = new MerchantDBManager())
            {
                merchantDBManager.SaveBusinessOwnerDetails(businessOwnerDetails);
            }
            return this.Ok("Tested Ok");
        }

        [Route("v1/Merchant/OwnerInformation")]
        public IHttpActionResult PostOwnerInformation(BusinessOwnerInformation request)
        {
            //MerchantWrapper merchantWrapper = new MerchantWrapper(this.GetAuthUID(), this.GetIdentityClaims(), this.Request.Headers);
            //return this.Ok(merchantWrapper.SaveNewMerchantInformation(new CommunicationsRq()));
            return this.Ok();

        }

        //public IHttpActionResult Get()
        //{
        //    return this.Ok();
        //}

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
    public class Request
    {
        public int CIS { get; set; }
        public string Token { get; set; }
        public int NID { get; set; }
        public string ProductID { get; set; }
    }
}
