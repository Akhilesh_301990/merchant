﻿using System.Web.Mvc;

namespace Nedbank.Web.API.Merchant.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
