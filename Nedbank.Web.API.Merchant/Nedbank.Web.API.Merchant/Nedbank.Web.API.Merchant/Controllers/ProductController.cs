﻿using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.Products;
using Nedbank.Web.API.Merchant.DTO.Application;
using System.Web.Http;

namespace Nedbank.Web.API.DMO.Controllers
{
    public class ProductController : ApiController//BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="leadMerchantInfo"></param>
        /// <returns></returns>
        [Route("v1/products/Add")]
        [HttpPost]
        public IHttpActionResult AddProduct(ProductInfo productInfo)
        {
            ProductWrapper productWrapper = new ProductWrapper();
            return this.Ok(productWrapper.AddProduct(productInfo));
            //return this.Response(productWrapper.AddProduct(productInfo));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="leadMerchantBusinessInfo"></param>
        /// <returns></returns>
        [Route("v1/products/{productCode}")]
        [HttpGet]
        public IHttpActionResult GetProductByProductCode(string productCode)
        {
            ProductWrapper productWrapper = new ProductWrapper();
            return this.Ok(productWrapper.GetProduct(productCode));
            //return this.Response(productWrapper.GetProduct(productCode));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="leadMerchantBusinessInfo"></param>
        /// <returns></returns>
        [Route("v1/products")]
        [HttpGet]
        public IHttpActionResult GetProductList()
        {
            ProductWrapper productWrapper = new ProductWrapper();
            return this.Ok(productWrapper.GetProduct(string.Empty));
            //return this.Response(productWrapper.GetProduct(productCode));
        }
    }
}
