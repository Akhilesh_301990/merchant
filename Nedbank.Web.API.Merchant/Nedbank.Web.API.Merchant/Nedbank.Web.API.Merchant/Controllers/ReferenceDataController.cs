﻿using Nedbank.Web.API.DMO.Models.ReferenceData;
using System.Web.Http;

namespace Nedbank.Web.API.DMO.Controllers
{
    public class ReferenceDataController : ApiController //BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("v1/industrycategories")]
        [HttpGet]
        public IHttpActionResult GetIndustryCategories()
        {
            ReferenceDataWrapper referenceDataWrapper = new ReferenceDataWrapper();
            return this.Ok(referenceDataWrapper.GetIndustryCategories());
            //return this.Response(referenceDataWrapper.GetIndustryCategories());
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="industryCategoryId"></param>
        /// <returns></returns>
        [Route("v1/industrysubcategories/{industryCategoryId}")]
        [HttpGet]
        public IHttpActionResult GetIndustrySubCategory(int industryCategoryId)
        {
            ReferenceDataWrapper referenceDataWrapper = new ReferenceDataWrapper();
            return this.Ok(referenceDataWrapper.GetIndustrySubCategory(industryCategoryId));
            //return this.Response(referenceDataWrapper.GetIndustrySubCategory(industryCategoryId));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("v1/businessprovince")]
        [HttpGet]
        public IHttpActionResult GetBusinessProvince()
        {
            ReferenceDataWrapper referenceDataWrapper = new ReferenceDataWrapper();
            return this.Ok(referenceDataWrapper.GetBusinessProvince());
            //return this.Response(referenceDataWrapper.GetBusinessProvince());
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("v1/citizenships")]
        [HttpGet]
        public IHttpActionResult GetCitizenships()
        {
            ReferenceDataWrapper referenceDataWrapper = new ReferenceDataWrapper();
            return this.Ok(referenceDataWrapper.GetCitizenships());
            //return this.Response(referenceDataWrapper.GetCitizenships());
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("v1/countries")]
        [HttpGet]
        public IHttpActionResult GetCountries()
        {
            ReferenceDataWrapper referenceDataWrapper = new ReferenceDataWrapper();
            return this.Ok(referenceDataWrapper.GetCountries());
            //return this.Response(referenceDataWrapper.GetCountries());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("v1/businesstypes")]
        [HttpGet]
        public IHttpActionResult GetBusinessTypes()
        {
            ReferenceDataWrapper referenceDataWrapper = new ReferenceDataWrapper();
            return this.Ok(referenceDataWrapper.GetBusinessTypes());
            //return this.Response(referenceDataWrapper.GetBusinessTypes());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="businessTypeId"></param>
        /// <returns></returns>
        [Route("v1/businesssubcategories")]
        [HttpGet]
        public IHttpActionResult GetBusinessSubCategory(int businessTypeId)
        {
            ReferenceDataWrapper referenceDataWrapper = new ReferenceDataWrapper();
            return this.Ok(referenceDataWrapper.GetBusinessSubCategory(businessTypeId));
            //return this.Response(referenceDataWrapper.GetBusinessSubCategory(businessTypeId));
        }
    }
}
