﻿using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nedbank.Web.API.DMO.Models
{
    public class DBErrorConfig
    {
        private static readonly Lazy<DBErrorConfig> lazy =
        new Lazy<DBErrorConfig>(() => new DBErrorConfig());

        private static Dictionary<int, ResultDetail> _databaseErrorDictionary = new Dictionary<int, ResultDetail>();

        public static DBErrorConfig Instance { get { return lazy.Value; } }

        public enum DatabaseErrorCodes
        {
            Success,
            DatabaseError,
            GeneralError
        }

        class StatusStrings
        {
            public const string SuccessMessage = "Success";
            public const string DatabaseErrorMessage = "Something went wrong while executing query.";
            public const string GeneralErrorMessage = "Failure";
        }

        public void Init()
        {
            _databaseErrorDictionary.Add((int)DatabaseErrorCodes.Success, new ResultDetail { Result = StatusCodes.SUCCESS, Reason = "Success", Status = StatusStrings.SuccessMessage });
            _databaseErrorDictionary.Add((int)DatabaseErrorCodes.DatabaseError, new ResultDetail { Result = StatusCodes.DATABASE_ERROR, Reason = "Failure", Status = StatusStrings.DatabaseErrorMessage });
            _databaseErrorDictionary.Add((int)DatabaseErrorCodes.GeneralError, new ResultDetail { Result = StatusCodes.GENERAL_ERROR, Reason = "Failure", Status = StatusStrings.GeneralErrorMessage });
        }

        public static Dictionary<int, ResultDetail> DatabaseErrorLookup
        {
            get
            {
                return _databaseErrorDictionary;
            }
        }
    }
}