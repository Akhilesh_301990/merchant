﻿using Nedbank.API.Documents;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http.Headers;
using API.BO.Communications;
using Nedbank.Web.API.Merchant.DTO.Application;
using Nedbank.Web.API.Merchant.Libraries;
using System.Collections;

namespace Nedbank.Web.API.Merchant.Models
{
    public class MerchantWrapper : APIBase
    {
        private static string BASEURL;
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountWrapper" /> class
        /// </summary>
        /// <param name="uid">user id fetched from token</param>
        /// <param name="identityClaims">The identity claims.</param>
        /// <param name="header">request header</param>
        public MerchantWrapper(string uid, Dictionary<string, string> identityClaims, HttpRequestHeaders header)
            : base(uid, identityClaims, header)
        {
            if (string.IsNullOrEmpty(BASEURL))
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["APIBaseUrl"]))
                    BASEURL = ConfigurationManager.AppSettings["APIBaseUrl"].ToString();
        }

        /// <summary>
        /// Refreshes the merchant account containers list in accounts cache.
        /// </summary>
        /// <returns>Merchant Refresh Status</returns>
        public APIResponse<string> RefreshMarchantInformation()
        {
            var account = new Merchant(this.Uid, this.IdentityClaims, this.Header);

            var result = "";// account.GetClientAccounts();

            var response = new APIResponse<string>();

            // var resultSets = new ResultSets();
            response.MetaData.ResultData = new ResultSets();

            ResultDetails resultDetails = new ResultDetails();
            if (result != null)
            {
                //response.Data = AccountErrorConfig.RefreshStatusSuccess;
                //response.MetaData.ResultData.Add(new ResultSet
                //{
                //    ResultDetail = new ResultDetails { AccountErrorConfig.GetReturnError("R00",
                //    AccountErrorConfig.HostOperation.ProfileGetInfo) }
                //});
                return response;
            }
            else
            {
                //response.Data = AccountErrorConfig.RefreshStatusSuccess;
                //response.MetaData.ResultData.Add(new ResultSet
                //{
                //    ResultDetail = new ResultDetails { AccountErrorConfig.GetReturnError("R02",
                //    AccountErrorConfig.HostOperation.ProfileGetInfo) }
                //});
                return response;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Accout Refresh Status</returns>
        public APIResponse<CommunicationsRs> SaveNewMerchantInformation(CommunicationsRq messageRq)
        {
            var response = new APIResponse<CommunicationsRs>() { Data = new CommunicationsRs(), MetaData = new Metadata() };
            var message = new Merchant(this.Uid, this.IdentityClaims, this.Header);
            var result = message.SendMessage(messageRq);
            //==> Add Data and MataData to response
            if (result.ReturnCode.Equals("R00"))
            {
                response.Data.ReferenceCode = result.ReferenceCode;
                response.Data.ResultMessage = result.ResultMessage;
                response.Data.ReturnCode = result.ReturnCode;
                response.Data.SourceSystemReference = result.SourceSystemReference;
                //response.MetaData.ResultData.Add(CommunicationsErrorConfigHost(result.ReturnCode.ToString()));
                return response;
            }
            else
            {
                //response.MetaData.ResultData.Add(CommunicationsErrorConfigHost(result.ReturnCode.ToString()));
                return response;
            }
        }

       
    }
}