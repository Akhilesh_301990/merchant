﻿using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.DBCommunication;
using Nedbank.Web.API.Merchant.DTO.Application;
using Nedbank.Web.API.Merchant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nedbank.Web.API.DMO.Models.ReferenceData
{
    public class ReferenceDataWrapper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public APIResponse<List<IndustyCategory>> GetIndustryCategories()
        {
            var response = new APIResponse<List<IndustyCategory>>();
            ResultDetails resultDetails;

            ReferenceDataDBManager referenceDataDBManager = new ReferenceDataDBManager();
            response.Data = referenceDataDBManager.GetIndustryCategories(out resultDetails);

            response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="industryCategoryId"></param>
        /// <returns></returns>
        public APIResponse<List<IndustrySubCategory>> GetIndustrySubCategory(int industryCategoryId)
        {
            var response = new APIResponse<List<IndustrySubCategory>>();
            ResultDetails resultDetails;

            ReferenceDataDBManager referenceDataDBManager = new ReferenceDataDBManager();
            response.Data = referenceDataDBManager.GetIndustrySubCategories(industryCategoryId,out resultDetails);

            response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public APIResponse<List<BusinessProvince>> GetBusinessProvince()
        {
            var response = new APIResponse<List<BusinessProvince>>();
            ResultDetails resultDetails;

            ReferenceDataDBManager referenceDataDBManager = new ReferenceDataDBManager();
            response.Data = referenceDataDBManager.GetBusinessProvince(out resultDetails);

            response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public APIResponse<List<Citizenship>> GetCitizenships()
        {
            var response = new APIResponse<List<Citizenship>>();
            ResultDetails resultDetails;

            ReferenceDataDBManager referenceDataDBManager = new ReferenceDataDBManager();
            response.Data = referenceDataDBManager.GetCitizenships(out resultDetails);

            response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public APIResponse<List<Country>> GetCountries()
        {
            var response = new APIResponse<List<Country>>();
            ResultDetails resultDetails;

            ReferenceDataDBManager referenceDataDBManager = new ReferenceDataDBManager();
            response.Data = referenceDataDBManager.GetCountries(out resultDetails);

            response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public APIResponse<List<BusinessType>> GetBusinessTypes()
        {
            var response = new APIResponse<List<BusinessType>>();
            ResultDetails resultDetails;

            ReferenceDataDBManager referenceDataDBManager = new ReferenceDataDBManager();
            response.Data = referenceDataDBManager.GetBusinessTypes(out resultDetails);

            response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="businessTypeId"></param>
        /// <returns></returns>
        public APIResponse<List<BusinessSubCategory>> GetBusinessSubCategory(int businessTypeId)
        {
            var response = new APIResponse<List<BusinessSubCategory>>();
            ResultDetails resultDetails;

            ReferenceDataDBManager referenceDataDBManager = new ReferenceDataDBManager();
            response.Data = referenceDataDBManager.GetBusinessSubCategory(businessTypeId, out resultDetails);

            response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);

            return response;
        }
    }
}