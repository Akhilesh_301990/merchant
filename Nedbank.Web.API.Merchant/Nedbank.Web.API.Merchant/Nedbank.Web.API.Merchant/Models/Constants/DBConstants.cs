﻿namespace Nedbank.Web.API.DMO.Models.Constants
{
    public class DBConstants
    {
        public const string PROC_SaveBusinessDetails = "PROC_SaveBusinessDetails";
        public const string PROC_SaveBusinessOwnerDetails = "PROC_SaveBusinessOwnerDetails";
    }
}