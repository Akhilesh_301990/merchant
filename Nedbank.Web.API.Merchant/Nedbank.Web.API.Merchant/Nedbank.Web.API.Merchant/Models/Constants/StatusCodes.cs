﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nedbank.Web.API.DMO.Models.Constants
{
    public class StatusCodes
    {
        public const string SUCCESS = "R00";
        public const string DATABASE_ERROR = "R01";
        public const string GENERAL_ERROR = "R02";
    }
}