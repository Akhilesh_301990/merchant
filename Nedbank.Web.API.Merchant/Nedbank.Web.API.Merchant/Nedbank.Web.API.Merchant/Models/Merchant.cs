﻿using API.BO.Communications;
//using Middleware;
//using Nedbank.API.BDM.Common;
//using Nedbank.API.BL;
using Nedbank.API.Documents;
using Nedbank.Proxies.OutputDistribution._20110801;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;

namespace Nedbank.Web.API.Merchant.Models
{
    public class Merchant : APIBase
    {
        readonly MerchantCommon common;

        /// <summary>
        /// DMOCommon constructor contains common object.
        /// </summary>
        /// <param name="uid">user id</param>
        /// <param name="identityClaims">The identity claims.</param>
        /// <param name="header">Request header.</param>
        internal Merchant(string uid, Dictionary<string, string> identityClaims, HttpRequestHeaders header)
            : base(uid, identityClaims, header)
        {
            this.common = new MerchantCommon(uid, identityClaims, header);
        }


        /// <summary>
        /// Send Email or SMS
        /// </summary>
        /// <param name="messageRq">Message request</param>
        /// <returns>Response</returns>
        public CommunicationsRs SendMessage(CommunicationsRq messageRq)
        {
            //===> Instence to interact with service
            IOutputDistributionChannel msgGateway = null;

            //===>CommunicationsRs: class generated from swagger class
            var response = new CommunicationsRs();
            try
            {
                //===> Call Service Factory code in proxy project to create instance
                //msgGateway = Proxies.OutputDistribution._20110801.ServiceFactory.CreateService(this.EnterpriseContext);

                //===> Call method in proxy folder
                //var msgGatewayRs = msgGateway.DistributeOutput(createOdsRequest(messageRq));
                //msgGateway.Close();

                //if (!msgGatewayRs.ReturnCode.Equals("R00", StringComparison.InvariantCultureIgnoreCase))
                //    instr.RaiseErrorEvent(string.Format("IOutputDistribution.DistributeOutput result {0} reason {1}", msgGatewayRs.ReturnCode, msgGatewayRs.ResultMessage), Infr.Instr.SDK.Events.EventSeverity.Error);

                //===> Assign response call 
                //response.ReturnCode = msgGatewayRs.ReturnCode;
                //response.ResultMessage = msgGatewayRs.ResultMessage;
                //response.ReferenceCode = msgGatewayRs.ReferenceCode;
                //response.SourceSystemReference = msgGatewayRs.SourceSystemReference;

                return response;
            }
            catch
            {
                if (msgGateway != null)
                {
                    //msgGateway.Abort();
                }
                throw;
            }
        }

        private DistributeOutputRequest createOdsRequest(CommunicationsRq CommunictionsRq)
        {
            try
            {
                var distributeMessage = new DistributeOutputRequest();
                //distributeMessage.EnterpriseContext = Mapper.MapToProxyEC(this.EnterpriseContext);
                if (CommunictionsRq.SourceSystemReference.Equals("MOD", StringComparison.InvariantCultureIgnoreCase))
                {
                    distributeMessage.ConfigurationReference = ConfigurationManager.AppSettings["ConfigurationReferenceForEmailMod"].ToString();
                    distributeMessage.Metadata = MetaData(CommunictionsRq);
                    distributeMessage.SourceSystemReference = ConfigurationManager.AppSettings["SourceSystemReference_SCP"].ToString();
                }
                else
                {
                    distributeMessage.OutputPayload = new OutputPayLoadType[1];
                    distributeMessage.OutputPayload[0] = new OutputPayLoadType();
                    OutputPayLoadType payload = new OutputPayLoadType();
                    if (CommunictionsRq.Channel.Equals("Email", StringComparison.InvariantCultureIgnoreCase) && CommunictionsRq.SourceSystemReference.Equals("gtretail", StringComparison.InvariantCultureIgnoreCase))
                    {
                        //generate datastring and metadata
                        distributeMessage.ConfigurationReference = ConfigurationManager.AppSettings["ConfigurationReference_Settlement"].ToString();
                        payload.DataString = GetDataString(CommunictionsRq);
                        distributeMessage.OutputPayload[0].DataString = payload.DataString;
                        distributeMessage.SourceSystemReference = CommunictionsRq.SourceSystemReference;
                    }
                    else
                    if (CommunictionsRq.Channel.Equals("Email", StringComparison.InvariantCultureIgnoreCase))
                    {
                        distributeMessage.ConfigurationReference = ConfigurationManager.AppSettings["ConfigurationReferenceForEmail"].ToString();
                        payload.DataString = ODSDataString(CommunictionsRq);
                        distributeMessage.OutputPayload[0].DataString = payload.DataString;
                        distributeMessage.SourceSystemReference = string.Format("{0}_{1}_{2}", CommunictionsRq.SourceSystemReference, CommunictionsRq.CustomerDetail[0].Identifier, System.DateTime.Now.ToString("MM/dd/yyyyHH:mm:ss"));
                    }
                    else
                    {
                        distributeMessage.ConfigurationReference = ConfigurationManager.AppSettings["ConfigurationReferenceForSMS"].ToString();
                        distributeMessage.SourceSystemReference = string.Empty;
                    }
                    distributeMessage.OutputPayload[0].MetadataKey = "1";
                    distributeMessage.Metadata = MetaData(CommunictionsRq);
                }

                return distributeMessage;
            }
            catch
            {
                throw;
            }

        }
        /// <summary>
        /// Get ODS data string for Settlement quote
        /// </summary>
        /// <param name="communicationsRq">reqeust</param>
        /// <returns>Data string</returns>
        private static string GetDataString(CommunicationsRq communicationsRq)
        {
            StringBuilder XmlDataString = new StringBuilder();
            string DataString = string.Empty;
            XmlDocument doc = TemplateXml(communicationsRq.MessageClassification);
            XmlNode emailNode = doc.DocumentElement.SelectSingleNode("//DataString");
            string dataString = emailNode.InnerXml;
            foreach (var Details in communicationsRq.ClassificationDetails)
            {
                if (!Details.TagName.Equals("ImpDate", StringComparison.InvariantCultureIgnoreCase) || !Details.TagName.Equals("EffectiveDate", StringComparison.InvariantCultureIgnoreCase) || !Details.TagName.Equals("ExpiryDate", StringComparison.InvariantCultureIgnoreCase)
                    || !Details.TagName.Equals("DocumentKey", StringComparison.InvariantCultureIgnoreCase) || !Details.TagName.Equals("CaseNo", StringComparison.InvariantCultureIgnoreCase) || !Details.TagName.Equals("RegNo", StringComparison.InvariantCultureIgnoreCase))
                {
                    XmlDataString.AppendFormat("<{0}>{1}</{2}>", Details.TagName, Details.TagValue, Details.TagName);
                }
            }

            foreach (var classificationDetails in communicationsRq.ClassificationDetails)
            {
                if (classificationDetails.TagName == "AcctNo")
                {
                    DataString = string.Format(dataString, XmlDataString.ToString());
                }
            }

            XmlDocument xmlConverter = new XmlDocument();
            xmlConverter.LoadXml(DataString);
            return xmlConverter.OuterXml;

        }

        /// <summary>
        /// Create metadata of ODS for email or sms
        /// </summary>
        /// <param name="communicationsRq">rrequest</param>
        /// <returns>data string</returns>
        private static string MetaData(CommunicationsRq communicationsRq)
        {
            StringBuilder toMail = new StringBuilder();
            string Client = string.Empty;
            string MetaData = string.Empty;
            XmlDocument doc = TemplateXml(communicationsRq.MessageClassification);
            switch (communicationsRq.Channel.ToLower())
            {
                case "email":
                    MetaData = string.Empty;
                    break;
                case "sms":
                    MetaData = CreateMetaData(communicationsRq, toMail, Client, MetaData, doc);
                    break;
                default:
                    break;
            }
            XmlDocument xmlConverter = new XmlDocument();
            xmlConverter.LoadXml(MetaData);
            return xmlConverter.OuterXml;
        }
        /// <summary>
        /// Create metadata for generic sms
        /// </summary>
        /// <param name="communicationsRq"></param>
        /// <param name="toMail"></param>
        /// <param name="Client"></param>
        /// <param name="MetaData"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        private static string CreateMetaData(CommunicationsRq communicationsRq, StringBuilder toMail, string Client, string MetaData, XmlDocument doc)
        {
            XmlNode emailNode = doc.DocumentElement.SelectSingleNode("//SMSMetadata");
            string SMS = emailNode.InnerXml;
            SMS = Regex.Replace(SMS, "{\\s+", "{");
            SMS = Regex.Replace(SMS, "\\s+}", "}");
            foreach (var recepientDetail in communicationsRq.RecipientDetails)
            {
                foreach (var contactDetails in recepientDetail.ContactDetail)
                {
                    toMail.AppendFormat("{0};", contactDetails.PhoneNumber);
                }
            }
            StringBuilder classficationDetails = new StringBuilder();
            foreach (var classficationDetail in communicationsRq.ClassificationDetails)
            {
                SMS = SMS.Replace("{" + classficationDetail.TagName.Trim() + "}", classficationDetail.TagValue);
            }
            foreach (var customerDetails in communicationsRq.CustomerDetail)
            {
                if (customerDetails.IdentifierType == "CIS")
                {
                    Client = string.Format("<CIS>{0}</CIS>", customerDetails.Identifier);
                }
                else
                {
                    Client = string.Format("<ID>{0}</ID>", customerDetails.Identifier);
                }
            }
            MetaData = string.Format(SMS, toMail.ToString(), Client);
            return MetaData;
        }

        private static string ODSDataString(CommunicationsRq communicationsRq)
        {
            StringBuilder XmlDataString = new StringBuilder();
            string DataString = string.Empty;
            XmlDocument doc = TemplateXml(communicationsRq.MessageClassification);
            XmlNode emailNode = doc.DocumentElement.SelectSingleNode("//DataString");
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            string dataString = emailNode.InnerXml;
            string Date = DateTime.Now.ToString("dd/MM/yyyy");
            foreach (var Details in communicationsRq.ClassificationDetails)
            {
                XmlDataString.AppendFormat("<{0}>{1}</{2}>", Details.TagName, Details.TagValue, Details.TagName);
            }
            foreach (var classificationDetails in communicationsRq.ClassificationDetails)
            {
                if (classificationDetails.TagName == "AccountName")
                {
                    DataString = string.Format(dataString, Date, XmlDataString.ToString(), communicationsRq.ClassificationDetails[0].TagValue);
                }
                else if (classificationDetails.TagName == "AcctNo")
                {
                    DataString = string.Format(dataString, Date, XmlDataString.ToString(), communicationsRq.ClassificationDetails[0].TagValue);
                }
            }
            XmlDocument xmlConverter = new XmlDocument();
            xmlConverter.LoadXml(DataString);
            string String = xmlConverter.OuterXml;
            return String;
        }

        /// <summary>
        /// Get xml template
        /// </summary>
        /// <param name="messageClassification">classification message name</param>
        /// <returns>xml template</returns>
        private static XmlDocument TemplateXml(string messageClassification)
        {
            string filePath = (ConfigurationManager.AppSettings["ConfigurationTemplate"].ToString() + messageClassification + ".xml");
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);
            return doc;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="header"></param>
        /// <param name="profileNumber"></param>
        /// <param name="accountNumber"></param>
        public void SendSMS(HttpRequestHeaders header, long profileNumber, string accountNumber)
        {
            try
            {
                //var clientDetails = PreferencesResource.GetClientDetails(profileNumber, header);
                //CommunicationsResource.Send<CommunicationsRs, CommunicationsRq>(CreateRequest(accountNumber, clientDetails.CellNumber, Convert.ToString(clientDetails.CisNumber)), header);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="cellNumber"></param>
        /// <param name="cisNumber"></param>
        /// <returns></returns>
        private static CommunicationsRq CreateRequest(string accountNumber, string cellNumber, string cisNumber)
        {
            CommunicationsRq communicationRq = new CommunicationsRq();
            communicationRq.Channel = "SMS";
            communicationRq.MessageClassification = "UpliftDormancy";
            communicationRq.SourceSystemReference = "UpliftDormancy";
            communicationRq.RecipientDetails = new List<RecipientDetails>();

            RecipientDetails recipientDetails = new RecipientDetails() { PartyName = string.Empty };
            recipientDetails.ContactDetail = new List<ContactDetails>();

            string phoneNumber = cellNumber.Substring(cellNumber.Length - 9);
            phoneNumber = "0" + phoneNumber;
            recipientDetails.ContactDetail.Add(new ContactDetails() { EmailId = string.Empty, PhoneNumber = phoneNumber });

            communicationRq.RecipientDetails.Add(recipientDetails);

            communicationRq.ClassificationDetails = new List<ClassificationDetails>();

            communicationRq.ClassificationDetails.Add(new ClassificationDetails()
            {
                TagName = "AccountNumber",
                TagValue = accountNumber
            });
            communicationRq.CustomerDetail = new List<CustomerDetails>();
            communicationRq.CustomerDetail.Add(new CustomerDetails() { Identifier = cisNumber, IdentifierType = "CIS" });
            return communicationRq;
        }
    }
}