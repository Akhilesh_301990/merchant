﻿using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.DBCommunication;
using Nedbank.Web.API.Merchant.DTO.Application;
using Nedbank.Web.API.Merchant.Models;
using System.Collections.Generic;

namespace Nedbank.Web.API.DMO.Models.Products
{
    public class ProductWrapper
    {
        /// <summary>
        /// This function faciliate the insertion of the product into the database table
        /// </summary>
        /// <param name="productInfo"></param>
        /// <returns></returns>
        public APIResponse<string> AddProduct(ProductInfo productInfo)
        {
            var response = new APIResponse<string>();

            ResultDetails resultDetails;

            ProductDBManager productDBManager = new ProductDBManager();
            productDBManager.AddProduct(productInfo, out resultDetails);

            response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);

            return response;
        }

        /// <summary>
        /// This function faciliate the retrival of the product details from the product code
        /// </summary>
        /// <param name="productCode"></param>
        /// <returns></returns>
        public APIResponse<List<ProductInfo>> GetProduct(string productCode)
        {
            var response = new APIResponse<List<ProductInfo>>();
            ResultDetails resultDetails;

            ProductDBManager productDBManager = new ProductDBManager();
            response.Data = productDBManager.GetProduct(productCode, out resultDetails);

            response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);

            return response;
        }
    }
}