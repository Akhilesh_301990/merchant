﻿using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.Providers;
using Nedbank.Web.API.Merchant.DTO.Application;
using System;
using System.Data.SqlClient;

namespace Nedbank.Web.API.DMO.Models.DBCommunication
{
    public class ProductCartDBManager
    {
        /// <summary>
        /// This function would insert a new record into the Product cart table.
        /// </summary>
        /// <param name="productInfo"></param>
        public Int64 AddProductIntoCart(ProductCartInfo productCartInfo, out ResultDetails resultDetails)
        {
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;

            try
            {
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);

                string sqlCommand = "INSERT INTO ProductCart (ProductCode, Quantity, TotalPrice, ReferenceId) VALUES (@ProductCode, @Quantity, @TotalPrice, @ReferenceId);";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);

                cmd.Parameters.AddWithValue("@ProductCode", productCartInfo.productCode);
                //cmd.Parameters.AddWithValue("@MerchantApplicationtId", productCartInfo.MerchantApplicationtId);
                cmd.Parameters.AddWithValue("@Quantity", productCartInfo.quantity);
                cmd.Parameters.AddWithValue("@TotalPrice", productCartInfo.totalPrice);
                cmd.Parameters.AddWithValue("@ReferenceId", productCartInfo.referenceId);

                sqlConnection.Open();
                cmd.ExecuteNonQuery();
                sqlConnection.Close();
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
            }
            catch (System.Exception ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
            }
            finally
            {
                sqlConnection.Close();
            }
            return productCartInfo.referenceId;
        }
    }
}