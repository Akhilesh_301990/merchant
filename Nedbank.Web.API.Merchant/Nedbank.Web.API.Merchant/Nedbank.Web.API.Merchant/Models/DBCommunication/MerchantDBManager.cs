﻿using Nedbank.Web.API.DMO.Models.Constants;
using Nedbank.Web.API.Merchant.DTO.Application;
using Nedbank.Web.API.Merchant.Libraries;
using System;
using System.Collections;
using System.IO;

namespace Nedbank.Web.API.DMO.Models.DBCommunication
{
    public class MerchantDBManager : IDisposable
    {
        Hashtable htParams;

        public int SaveBusinessDetails(BusinessOwnerInformation businessOwnerInformation)
        {
            try
            {
                File.WriteAllText(@"D:\Debugging.txt", "Start");
                htParams = new Hashtable();
                htParams.Add("@BusinessName", businessOwnerInformation.businessName);
                htParams.Add("@Industry", businessOwnerInformation.industry);
                htParams.Add("@IndustrySubCategoryType", businessOwnerInformation.industrySubCategoryType);
                htParams.Add("@BusinessLocation", businessOwnerInformation.businessLocation);
                htParams.Add("@AlternateContact", businessOwnerInformation.alternateContact);
                htParams.Add("@MonthlyTurnover", businessOwnerInformation.monthlyTurnover);
                htParams.Add("@Result", 0);
                File.WriteAllText(@"D:\Debugging.txt", "DbConnection");
                DBUtility.Instance("DigiMob").ExecuteQueries(DBConstants.PROC_SaveBusinessDetails, 1, 5, 0, false, htParams);
                File.WriteAllText(@"D:\Debugging.txt", "End DB ");

            }
            catch (Exception ex)
            {
                File.WriteAllText(@"D:\Debugging.txt", ex.Message);
                throw;
            }
            return 1;
        }

        public int SaveBusinessOwnerDetails(BusinessOwnerDetails businessOwnerDetails)
        {
            try
            {
                File.WriteAllText(@"D:\Debugging.txt", "Start");
                htParams = new Hashtable();
                htParams.Add("@title", businessOwnerDetails.title);
                htParams.Add("@firstName", businessOwnerDetails.firstName);
                htParams.Add("@surName", businessOwnerDetails.surname);
                htParams.Add("@primaryContactNumber", businessOwnerDetails.primaryContactNumber);
                htParams.Add("@email", businessOwnerDetails.email);
                htParams.Add("@Result", 0);
                File.WriteAllText(@"D:\Debugging.txt", "DbConnection");
                object output = DBUtility.Instance("DigiMob").ExecuteQueries(DBConstants.PROC_SaveBusinessOwnerDetails, 1, 5, 0, false, htParams);
                File.WriteAllText(@"D:\Debugging.txt", "End DB ");

            }
            catch (Exception ex)
            {
                File.WriteAllText(@"D:\Debugging.txt", ex.Message);
                throw;
            }
            return 1;
        }
        #region Implementing IDisposible
        /// <summary>
        /// Method to Dispose the class
        /// </summary>        
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}