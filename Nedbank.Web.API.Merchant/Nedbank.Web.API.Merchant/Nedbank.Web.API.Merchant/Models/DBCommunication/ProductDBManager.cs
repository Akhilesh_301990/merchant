﻿using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.Providers;
using Nedbank.Web.API.Merchant.DTO.Application;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Nedbank.Web.API.DMO.Models.DBCommunication
{
    public class ProductDBManager
    {
        /// <summary>
        /// This function would insert a new record into the Product table.
        /// </summary>
        /// <param name="productInfo"></param>
        /// <param name="resultDetails"></param>
        public void AddProduct(ProductInfo productInfo, out ResultDetails resultDetails)
        {
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;

            try
            {
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);

                string sqlCommand = "INSERT INTO ProductDetails (ProductCode, ProductName, ProductDescription, Price) VALUES (@ProductCode, @ProductName, @ProductDescription, @Price);";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);

                cmd.Parameters.AddWithValue("@ProductCode", productInfo.productCode);
                cmd.Parameters.AddWithValue("@ProductName", productInfo.productName);
                cmd.Parameters.AddWithValue("@ProductDescription", productInfo.productDescription);
                cmd.Parameters.AddWithValue("@Price", productInfo.price);

                sqlConnection.Open();
                cmd.ExecuteNonQuery();
                sqlConnection.Close();
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
            }
            catch (System.Exception ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        /// <summary>
        /// This function return the product details from the given product code.
        /// </summary>
        /// <param name="productCode"></param>
        /// <param name="resultDetails"></param>
        /// <returns></returns>
        public List<ProductInfo> GetProduct(string productCode, out ResultDetails resultDetails)
        {
            List<ProductInfo> productInfoList = null;
            ProductInfo productInfo = null;
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);

                string sqlCommand = string.IsNullOrEmpty(productCode) ? "SELECT * FROM ProductDetails;" : "SELECT * FROM ProductDetails WHERE ProductCode = @ProductCode;";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);

                if (!string.IsNullOrEmpty(productCode))
                    cmd.Parameters.AddWithValue("@ProductCode", productCode);

                sqlConnection.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    productInfoList = new List<ProductInfo>();

                    while (reader.Read())
                    {
                        productInfo = new ProductInfo();
                        productInfo.productId = (int)reader["ProductId"];
                        productInfo.productName = Convert.ToString(reader["ProductName"]);
                        productInfo.productDescription = Convert.ToString(reader["ProductDescription"]);
                        productInfo.productCode = Convert.ToString(reader["ProductCode"]);
                        productInfo.price = (int)reader["Price"];
                        productInfo.priceDescription = Convert.ToString(reader["PriceDescription"]);
                        productInfoList.Add(productInfo);
                    }
                }
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
            }
            catch (System.Exception ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
            }
            finally
            {
                sqlConnection.Close();
            }
            return productInfoList;
        }
    }
}