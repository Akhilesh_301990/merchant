﻿using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.Providers;
using Nedbank.Web.API.Merchant.DTO.Application;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Nedbank.Web.API.DMO.Models.DBCommunication
{
    public class LeadFormDBManager : IDisposable
    {
        public LeadFormDBManager()
        {

        }

        /// <summary>
        /// This function would insert a new record into the MerchatLeadDetails table.
        /// </summary>
        /// <param name="leadMerchantInfo"></param>
        /// <param name="resultDetails"></param>
        public int AddMerchant(LeadMerchantInfo leadMerchantInfo, out ResultDetails resultDetails)
        {
            object ReturnValue = 0;
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;
            //StringBuilder error= new StringBuilder();
            try
            {
                //error.AppendLine("Get Con");
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);

                string sqlCommand = "INSERT INTO MerchantLeadDetails (Title,Firstname,Surname,EmailAddress,PrimaryContactNumber,CreatedOn,UpdatedOn,IsDeleted) VALUES (@Title,@Firstname,@Surname,@EmailAddress,@PrimaryContactNumber,@CreatedOn,@UpdatedOn,@IsDeleted);" + "Select Scope_Identity();";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);
                //error.AppendLine("Add Param");
                cmd.Parameters.AddWithValue("@Title", leadMerchantInfo.title);
                cmd.Parameters.AddWithValue("@Firstname", leadMerchantInfo.firstname);
                cmd.Parameters.AddWithValue("@Surname", leadMerchantInfo.surname);
                cmd.Parameters.AddWithValue("@EmailAddress", leadMerchantInfo.emailAddress);
                cmd.Parameters.AddWithValue("@PrimaryContactNumber", leadMerchantInfo.primaryContactNumber);
                //cmd.Parameters.AddWithValue("@AlternateContactNumber", leadMerchantInfo.AlternateContactNumber);
                cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@IsDeleted", 0);
                //error.AppendLine("Open Con");
                sqlConnection.Open();
                //error.AppendLine("Save call");
                ReturnValue = cmd.ExecuteScalar();
                sqlConnection.Close();
                //error.AppendLine("Closed");
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                //error.AppendLine(ex.Message);
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
            }
            catch (System.Exception ex)
            {
                //error.AppendLine(ex.Message);
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
            }
            finally
            {
                sqlConnection.Close();
            }
            //error.AppendLine("Completed");
            //File.WriteAllText(@"D:\LogFile.txt", error.ToString());
            return Convert.ToInt32(ReturnValue);
        }


        /// <summary>
        /// This function would insert a new record into the MerchantLeadBusinessDetails table
        /// </summary>
        /// <param name="leadMerchantBusinessInfo"></param>
        /// <param name="resultDetails"></param>
        public string AddNewMerchantBusinessInfo(LeadMerchantBusinessInfo leadMerchantBusinessInfo, out ResultDetails resultDetails)
        {
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;
            object ReturnValue = 0;
            string ReferenceNumber = Convert.ToString(Guid.NewGuid()).Replace("-", string.Empty);

            try
            {
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);
                string ss = Convert.ToString(Guid.NewGuid());

                string sqlCommand = "INSERT INTO MerchantLeadBusinessDetails (MerchantLeadId,BusinessName,IndustryCategory,IndustySubCategory,BusinessProvience,MonthlyTurnover,ReferenceNumber,CreatedOn,UpdatedOn,IsDeleted) VALUES (@MerchantLeadId,@BusinessName,@IndustryCategory,@IndustySubCategory,@BusinessProvience,@MonthlyTurnover,@ReferenceNumber,@CreatedOn,@UpdatedOn,@IsDeleted);"
                                    + "Select Scope_Identity();"
                                    + "UPDATE MerchantLeadDetails SET AlternateContactNumber = @AlternateContactNumber WHERE MerchantLeadId = @MerchantLeadId;";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);

                cmd.Parameters.AddWithValue("@MerchantLeadId", leadMerchantBusinessInfo.merchantLeadId);
                cmd.Parameters.AddWithValue("@BusinessName", leadMerchantBusinessInfo.businessName);
                cmd.Parameters.AddWithValue("@IndustryCategory", leadMerchantBusinessInfo.industryCategory);
                cmd.Parameters.AddWithValue("@IndustySubCategory", leadMerchantBusinessInfo.industySubCategory);
                cmd.Parameters.AddWithValue("@BusinessProvience", leadMerchantBusinessInfo.businessProvience);
                cmd.Parameters.AddWithValue("@MonthlyTurnover", leadMerchantBusinessInfo.monthlyTurnover);
                cmd.Parameters.AddWithValue("@AlternateContactNumber", leadMerchantBusinessInfo.alternateContact);
                cmd.Parameters.AddWithValue("@ReferenceNumber", ReferenceNumber);
                cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
                cmd.Parameters.AddWithValue("@IsDeleted", 0);

                sqlConnection.Open();
                ReturnValue = cmd.ExecuteScalar();
                sqlConnection.Close();
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
                ReferenceNumber = string.Empty;
            }
            catch (System.Exception ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
                ReferenceNumber = string.Empty;
            }
            finally
            {
                sqlConnection.Close();
            }
            return ReferenceNumber;
        }

        #region Implementing IDisposible
        /// <summary>
        /// Method to Dispose the class
        /// </summary>        
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}