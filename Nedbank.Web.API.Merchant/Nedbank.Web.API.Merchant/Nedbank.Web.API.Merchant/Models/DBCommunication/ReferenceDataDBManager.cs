﻿using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.Providers;
using Nedbank.Web.API.Merchant.DTO.Application;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Nedbank.Web.API.DMO.Models.DBCommunication
{
    public class ReferenceDataDBManager : IDisposable
    {
        #region Implementing IDisposible
        /// <summary>
        /// Method to Dispose the class
        /// </summary>        
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion

        /// <summary>
        /// This function returns collection of all industy cateogires
        /// </summary>
        /// <param name="resultDetails"></param>
        /// <returns></returns>
        public List<IndustyCategory> GetIndustryCategories(out ResultDetails resultDetails)
        {
            List<IndustyCategory> industryCategories = null;
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);

                string sqlCommand = "SELECT * FROM IndustryCategory";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);

                sqlConnection.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    IndustyCategory industyCategory;
                    industryCategories = new List<IndustyCategory>();

                    while (reader.Read())
                    {
                        industyCategory = new IndustyCategory();
                        industyCategory.industryCategoryId = (int)reader["IndustryCategoryId"];
                        industyCategory.industryCategoryName = reader["IndustryCategoryName"].ToString();

                        industryCategories.Add(industyCategory);
                    }
                }
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
            }
            catch (System.Exception ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
            }
            finally
            {
                sqlConnection.Close();
            }
            return industryCategories;
        }


        /// <summary>
        /// This function returns the industry sub categories based on the given industry cateogory
        /// </summary>
        /// <param name="industryCategoryId"></param>
        /// <param name="resultDetails"></param>
        /// <returns></returns>
        public List<IndustrySubCategory> GetIndustrySubCategories(int industryCategoryId, out ResultDetails resultDetails)
        {
            List<IndustrySubCategory> industrySubCategories = null;
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);

                string sqlCommand = "SELECT * FROM IndustySubCategory where IndustryCategoryId = @IndustryCategoryId";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);

                cmd.Parameters.AddWithValue("@IndustryCategoryId", industryCategoryId);

                sqlConnection.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    IndustrySubCategory subCategory;
                    industrySubCategories = new List<IndustrySubCategory>();

                    while (reader.Read())
                    {
                        subCategory = new IndustrySubCategory();
                        subCategory.industySubCategoryId = (int)reader["IndustySubCategoryId"];
                        subCategory.industySubCategoryName = reader["IndustySubCategoryName"].ToString();

                        industrySubCategories.Add(subCategory);
                    }
                }

                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
            }
            catch (System.Exception ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
            }
            finally
            {
                sqlConnection.Close();
            }
            return industrySubCategories;
        }

        /// <summary>
        /// This function returns the collection of all province
        /// </summary>
        /// <param name="resultDetails"></param>
        /// <returns></returns>
        public List<BusinessProvince> GetBusinessProvince(out ResultDetails resultDetails)
        {
            List<BusinessProvince> businessProvince = null;
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);

                string sqlCommand = "SELECT * FROM Province";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);

                sqlConnection.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    BusinessProvince subCategory;
                    businessProvince = new List<BusinessProvince>();

                    while (reader.Read())
                    {
                        subCategory = new BusinessProvince();
                        subCategory.provinceId = (int)reader["ProvinceId"];
                        subCategory.provinceName = Convert.ToString(reader["ProvinceName"]);
                        subCategory.provinceCode = Convert.ToString(reader["ProvinceCode"]);

                        businessProvince.Add(subCategory);
                    }
                }

                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
            }
            catch (Exception ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
            }
            finally
            {
                sqlConnection.Close();
            }
            return businessProvince;
        }

        /// <summary>
        /// This function returns the collection of all citizenships
        /// </summary>
        /// <param name="resultDetails"></param>
        /// <returns></returns>
        public List<Citizenship> GetCitizenships(out ResultDetails resultDetails)
        {
            List<Citizenship> citizenships = null;
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);

                string sqlCommand = "SELECT * FROM Citizenship";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);

                sqlConnection.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    Citizenship citizenship;
                    citizenships = new List<Citizenship>();

                    while (reader.Read())
                    {
                        citizenship = new Citizenship();
                        citizenship.citizenShipId = (int)reader["Citizenship"];
                        citizenship.citizenShipName = Convert.ToString(reader["CitizenShipName"]);

                        citizenships.Add(citizenship);
                    }
                }

                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
            }
            catch (Exception ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
            }
            finally
            {
                sqlConnection.Close();
            }
            return citizenships;
        }


        /// <summary>
        /// This function returns collection of all contries
        /// </summary>
        /// <param name="resultDetails"></param>
        /// <returns></returns>
        public List<Country> GetCountries(out ResultDetails resultDetails)
        {
            List<Country> countries = null;
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);

                string sqlCommand = "SELECT * FROM Countries";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);

                sqlConnection.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    Country country;
                    countries = new List<Country>();

                    while (reader.Read())
                    {
                        country = new Country();
                        country.countryId = (int)reader["CountryId"];
                        country.countryName = Convert.ToString(reader["CountryName"]);
                        country.countryCode = Convert.ToString(reader["CountryCode"]);

                        countries.Add(country);
                    }
                }

                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
            }
            catch (Exception ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
            }
            finally
            {
                sqlConnection.Close();
            }
            return countries;
        }


        /// <summary>
        /// This function returns the collection of all business types
        /// </summary>
        /// <param name="resultDetails"></param>
        /// <returns></returns>
        public List<BusinessType> GetBusinessTypes(out ResultDetails resultDetails)
        {
            List<BusinessType> businessTypes = null;
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);

                string sqlCommand = "SELECT * FROM BusinessTypes";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);

                sqlConnection.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    BusinessType businessType;
                    businessTypes = new List<BusinessType>();

                    while (reader.Read())
                    {
                        businessType = new BusinessType();
                        businessType.businessTypeId = (int)reader["BusinessTypeId"];
                        businessType.businessTypeName = Convert.ToString(reader["BusinessTypeName"]);

                        businessTypes.Add(businessType);
                    }
                }

                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
            }
            catch (Exception ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
            }
            finally
            {
                sqlConnection.Close();
            }
            return businessTypes;
        }


        /// <summary>
        /// This function returns the business sub categories based on the given business type
        /// </summary>
        /// <param name="businessTypeId"></param>
        /// <param name="resultDetails"></param>
        /// <returns></returns>
        public List<BusinessSubCategory> GetBusinessSubCategory(int businessTypeId, out ResultDetails resultDetails)
        {
            List<BusinessSubCategory> businessSubCategories = null;
            resultDetails = new ResultDetails();
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(ConfigurationProvider.Instance.sqlConnectionString);

                string sqlCommand = "SELECT * FROM BusinessSubCategory where BusinessTypeId = @BusinessTypeId";
                SqlCommand cmd = new SqlCommand(sqlCommand, sqlConnection);
                cmd.Parameters.AddWithValue("@BusinessTypeId", businessTypeId);

                sqlConnection.Open();
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    BusinessSubCategory businessSubCategory;
                    businessSubCategories = new List<BusinessSubCategory>();

                    while (reader.Read())
                    {
                        businessSubCategory = new BusinessSubCategory();
                        businessSubCategory.businessSubCategoryId = (int)reader["BusinessSubCategoryId"];
                        businessSubCategory.businessSubCategoryName = Convert.ToString(reader["BusinessSubCategoryName"]);
                        businessSubCategory.businessTypeId = (int)reader["BusinessTypeId"];
                        businessSubCategories.Add(businessSubCategory);
                    }
                }

                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.Success]);
            }
            catch (SqlException ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.DatabaseError]);
            }
            catch (Exception ex)
            {
                resultDetails.Add(DBErrorConfig.DatabaseErrorLookup[(int)DBErrorConfig.DatabaseErrorCodes.GeneralError]);
            }
            finally
            {
                sqlConnection.Close();
            }
            return businessSubCategories;
        }

    }
}