﻿using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.DBCommunication;
using Nedbank.Web.API.Merchant.DTO.Application;
using Nedbank.Web.API.Merchant.Models;
using System;

namespace Nedbank.Web.API.DMO.Models.MerchantLead
{
    public class MerchantLeadWrapper
    {
        //public MerchantLeadWrapper(string uid, Dictionary<string, string> identityClaims, HttpRequestHeaders header)
        //    : base(uid, identityClaims, header)
        //{

        //}

        /// <summary>
        /// This function facilite the insertion of the Merchant info into the database table 
        /// </summary>
        /// <param name="leadMerchantInfo"></param>
        /// <returns></returns>
        public APIResponse<int> AddMerchantLeadInformation(LeadMerchantInfo leadMerchantInfo)
        {
            var response = new APIResponse<int>();
            ResultDetails resultDetails = new ResultDetails();
            LeadFormDBManager leadFormDBManager = new LeadFormDBManager();

            response.Data = leadFormDBManager.AddMerchant(leadMerchantInfo, out resultDetails);

            response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);
            return response;
        }


        /// <summary>
        /// This function facilite the insertion of the Merchant business info into the database table
        /// </summary>
        /// <param name="leadMerchantBusinessInfo"></param>
        /// <returns></returns>
        public APIResponse<string> AddMerchantLeadBusinessInformation(LeadMerchantBusinessInfo leadMerchantBusinessInfo)
        {
            var response = new APIResponse<string>();

            if (leadMerchantBusinessInfo.merchantLeadId <= 0)
                return response;
            
            ResultDetails resultDetails = new ResultDetails();
            using (LeadFormDBManager leadFormDBManager = new LeadFormDBManager())
            {
                response.Data = leadFormDBManager.AddNewMerchantBusinessInfo(leadMerchantBusinessInfo, out resultDetails);
                response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);
            }

            return response;
        }
    }
}