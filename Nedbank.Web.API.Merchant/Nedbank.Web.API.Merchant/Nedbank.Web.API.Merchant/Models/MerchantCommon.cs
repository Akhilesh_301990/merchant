﻿using Nedbank.API.Documents;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace Nedbank.Web.API.Merchant.Models
{
    public class MerchantCommon : APIBase
    {
        //public const string st_BaseURI = "v1/DMO/";
        //public const string st_GetRefreshURI = "Test";
        //public const string st_GetMerchantInfoURI = "Test/Test2";

        public const string st_GetRefreash = "Refresh";
        public const string st_GetMerchantDetails = "GetMerchantDetails";
        public const string st_Validate = "Validate";

        /// <summary>
        /// Initializes a new instance of the <see cref="MerchantCommon" /> class
        /// </summary>
        /// <param name="uid">user id fetched from token</param>
        /// <param name="identityClaims">The identity claims.</param>
        /// <param name="header">Request header.</param>
        public MerchantCommon(string uid, Dictionary<string, string> identityClaims, HttpRequestHeaders header)
            : base(uid, identityClaims, header)
        {

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultDetails"></param>
        /// <returns></returns>
        public static ResultSets InitResultSet(ResultDetails resultDetails)
        {
            var resultSets = new ResultSets();

            ResultSet set = new ResultSet();
            resultSets.Add(set);

            set.ResultDetail = resultDetails;

            return resultSets;
        }
    }
}