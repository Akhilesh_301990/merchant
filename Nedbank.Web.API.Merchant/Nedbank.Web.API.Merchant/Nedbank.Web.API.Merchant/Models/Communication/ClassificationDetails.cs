using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace API.BO.Communications
{
    /// <summary>
    /// ClassificationDetails
    /// </summary>
    [DataContract]
    public partial class ClassificationDetails :  IEquatable<ClassificationDetails>, IValidatableObject
    {

        public ClassificationDetails()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassificationDetails" /> class.
        /// </summary>
        /// <param name="TagName">tag name for the particular request in the form of eg. xml tag.</param>
        /// <param name="TagValue">value which needs to be send according to the api call..</param>
        public ClassificationDetails(string TagName = default(string), string TagValue = default(string))
        {
            this.TagName = TagName;
            this.TagValue = TagValue;
        }
        
        /// <summary>
        /// tag name for the particular request in the form of eg. xml tag
        /// </summary>
        /// <value>tag name for the particular request in the form of eg. xml tag</value>
        [Required]
        [DataMember(Name="tagName", EmitDefaultValue=false)]
        public string TagName { get; set; }
        /// <summary>
        /// value which needs to be send according to the api call.
        /// </summary>
        /// <value>value which needs to be send according to the api call.</value>
        [Required]
        [DataMember(Name="tagValue", EmitDefaultValue=false)]
        public string TagValue { get; set; }
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ClassificationDetails {\n");
            sb.Append("  TagName: ").Append(TagName).Append("\n");
            sb.Append("  TagValue: ").Append(TagValue).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            return this.Equals(obj as ClassificationDetails);
        }

        /// <summary>
        /// Returns true if ClassificationDetails instances are equal
        /// </summary>
        /// <param name="other">Instance of ClassificationDetails to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ClassificationDetails other)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            if (other == null)
                return false;

            return 
                (
                    this.TagName == other.TagName ||
                    this.TagName != null &&
                    this.TagName.Equals(other.TagName)
                ) && 
                (
                    this.TagValue == other.TagValue ||
                    this.TagValue != null &&
                    this.TagValue.Equals(other.TagValue)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                if (this.TagName != null)
                    hash = hash * 59 + this.TagName.GetHashCode();
                if (this.TagValue != null)
                    hash = hash * 59 + this.TagValue.GetHashCode();
                return hash;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        { 
            yield break;
        }
    }

}
