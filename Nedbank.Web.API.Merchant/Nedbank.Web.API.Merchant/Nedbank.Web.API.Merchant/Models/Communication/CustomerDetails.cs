using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace API.BO.Communications
{
    /// <summary>
    /// CustomerDetails
    /// </summary>
    [DataContract]
    public partial class CustomerDetails :  IEquatable<CustomerDetails>, IValidatableObject
    {

        public CustomerDetails()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerDetails" /> class.
        /// </summary>
        /// <param name="IdentifierType">CIS number or Id number..</param>
        /// <param name="Identifier">CIS number.</param>
        public CustomerDetails(string IdentifierType = default(string), string Identifier = default(string))
        {
            this.IdentifierType = IdentifierType;
            this.Identifier = Identifier;
        }
        
        /// <summary>
        /// CIS number or Id number.
        /// </summary>
        /// <value>CIS number or Id number.</value>
        [Required]
        [DataMember(Name="identifierType", EmitDefaultValue=false)]
        public string IdentifierType { get; set; }
        /// <summary>
        /// CIS number
        /// </summary>
        /// <value>CIS number</value>
        [Required]
        [DataMember(Name="identifier", EmitDefaultValue=false)]
        public string Identifier { get; set; }
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class CustomerDetails {\n");
            sb.Append("  IdentifierType: ").Append(IdentifierType).Append("\n");
            sb.Append("  Identifier: ").Append(Identifier).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            return this.Equals(obj as CustomerDetails);
        }

        /// <summary>
        /// Returns true if CustomerDetails instances are equal
        /// </summary>
        /// <param name="other">Instance of CustomerDetails to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(CustomerDetails other)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            if (other == null)
                return false;

            return 
                (
                    this.IdentifierType == other.IdentifierType ||
                    this.IdentifierType != null &&
                    this.IdentifierType.Equals(other.IdentifierType)
                ) && 
                (
                    this.Identifier == other.Identifier ||
                    this.Identifier != null &&
                    this.Identifier.Equals(other.Identifier)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                if (this.IdentifierType != null)
                    hash = hash * 59 + this.IdentifierType.GetHashCode();
                if (this.Identifier != null)
                    hash = hash * 59 + this.Identifier.GetHashCode();
                return hash;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        { 
            yield break;
        }
    }

}
