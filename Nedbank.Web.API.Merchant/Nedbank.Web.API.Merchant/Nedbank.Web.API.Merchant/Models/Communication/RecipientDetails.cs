using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace API.BO.Communications
{
    /// <summary>
    /// RecipientDetails
    /// </summary>
    [DataContract]
    public partial class RecipientDetails :  IEquatable<RecipientDetails>, IValidatableObject
    {
        public RecipientDetails()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecipientDetails" /> class.
        /// </summary>
        /// <param name="PartyName">name of the user to send the mail..</param>
        /// <param name="ContactDetail">Email id or Cell phone number (depending on Channel)..</param>
        public RecipientDetails(string PartyName = default(string), List<ContactDetails> ContactDetail = default(List<ContactDetails>))
        {
            this.PartyName = PartyName;
            this.ContactDetail = ContactDetail;
        }
        
        /// <summary>
        /// name of the user to send the mail.
        /// </summary>
        /// <value>name of the user to send the mail.</value>
        [DataMember(Name="partyName", EmitDefaultValue=false)]
        public string PartyName { get; set; }
        /// <summary>
        /// Email id or Cell phone number (depending on Channel).
        /// </summary>
        /// <value>Email id or Cell phone number (depending on Channel).</value>
        [DataMember(Name="contactDetail", EmitDefaultValue=false)]
        public List<ContactDetails> ContactDetail { get; set; }
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class RecipientDetails {\n");
            sb.Append("  PartyName: ").Append(PartyName).Append("\n");
            sb.Append("  ContactDetail: ").Append(ContactDetail).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            return this.Equals(obj as RecipientDetails);
        }

        /// <summary>
        /// Returns true if RecipientDetails instances are equal
        /// </summary>
        /// <param name="other">Instance of RecipientDetails to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(RecipientDetails other)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            if (other == null)
                return false;

            return 
                (
                    this.PartyName == other.PartyName ||
                    this.PartyName != null &&
                    this.PartyName.Equals(other.PartyName)
                ) && 
                (
                    this.ContactDetail == other.ContactDetail ||
                    this.ContactDetail != null &&
                    this.ContactDetail.SequenceEqual(other.ContactDetail)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                if (this.PartyName != null)
                    hash = hash * 59 + this.PartyName.GetHashCode();
                if (this.ContactDetail != null)
                    hash = hash * 59 + this.ContactDetail.GetHashCode();
                return hash;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        { 
            yield break;
        }
    }

}
