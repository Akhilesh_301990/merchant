using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace API.BO.Communications
{
    /// <summary>
    /// CommunicationsRq
    /// </summary>
    [DataContract]
    public partial class CommunicationsRq : IEquatable<CommunicationsRq>, IValidatableObject
    {


        public CommunicationsRq()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationsRq" /> class.
        /// </summary>
        /// <param name="MessageClassification">to identify Caller API..</param>
        /// <param name="SourceSystemReference">reference of the caller api with client ID..</param>
        /// <param name="Channel">Means of communication (Email/Sms).</param>
        /// <param name="ClassificationDetails">calling api details are stored in the form of key value here..</param>
        /// <param name="RecipientDetails">contact information..</param>
        /// <param name="CustomerDetail">customer details..</param>
        public CommunicationsRq(string MessageClassification = default(string), string SourceSystemReference = default(string), string Channel = default(string), List<ClassificationDetails> ClassificationDetails = default(List<ClassificationDetails>), List<RecipientDetails> RecipientDetails = default(List<RecipientDetails>), List<CustomerDetails> CustomerDetail = default(List<CustomerDetails>))
        {
            this.MessageClassification = MessageClassification;
            this.SourceSystemReference = SourceSystemReference;
            this.Channel = Channel;
            this.ClassificationDetails = ClassificationDetails;
            this.RecipientDetails = RecipientDetails;
            this.CustomerDetail = CustomerDetail;
        }

        /// <summary>
        /// to identify Caller API.
        /// </summary>
        /// <value>to identify Caller API.</value>
        [Required]
        [DataMember(Name = "messageClassification", EmitDefaultValue = false)]
        public string MessageClassification { get; set; }
        /// <summary>
        /// reference of the caller api with client ID.
        /// </summary>
        /// <value>reference of the caller api with client ID.</value>
        [Required]
        [DataMember(Name = "sourceSystemReference", EmitDefaultValue = false)]
        public string SourceSystemReference { get; set; }
        /// <summary>
        /// Means of communication (Email/Sms)
        /// </summary>
        /// <value>Means of communication (Email/Sms)</value>
        [Required]
        [DataMember(Name = "channel", EmitDefaultValue = false)]
        public string Channel { get; set; }
        /// <summary>
        /// calling api details are stored in the form of key value here.
        /// </summary>
        /// <value>calling api details are stored in the form of key value here.</value>
        [Required]
        [DataMember(Name = "classificationDetails", EmitDefaultValue = false)]
        public List<ClassificationDetails> ClassificationDetails { get; set; }
        /// <summary>
        /// contact information.
        /// </summary>
        /// <value>contact information.</value>
        [Required]
        [DataMember(Name = "recipientDetails", EmitDefaultValue = false)]
        public List<RecipientDetails> RecipientDetails { get; set; }
        /// <summary>
        /// customer details.
        /// </summary>
        /// <value>customer details.</value>
        [DataMember(Name = "customerDetail", EmitDefaultValue = false)]
        public List<CustomerDetails> CustomerDetail { get; set; }
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class CommunicationsRq {\n");
            sb.Append("  MessageClassification: ").Append(MessageClassification).Append("\n");
            sb.Append("  SourceSystemReference: ").Append(SourceSystemReference).Append("\n");
            sb.Append("  Channel: ").Append(Channel).Append("\n");
            sb.Append("  ClassificationDetails: ").Append(ClassificationDetails).Append("\n");
            sb.Append("  RecipientDetails: ").Append(RecipientDetails).Append("\n");
            sb.Append("  CustomerDetail: ").Append(CustomerDetail).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            return this.Equals(obj as CommunicationsRq);
        }

        /// <summary>
        /// Returns true if CommunicationsRq instances are equal
        /// </summary>
        /// <param name="other">Instance of CommunicationsRq to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(CommunicationsRq other)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            if (other == null)
                return false;

            return
                (
                    this.MessageClassification == other.MessageClassification ||
                    this.MessageClassification != null &&
                    this.MessageClassification.Equals(other.MessageClassification)
                ) &&
                (
                    this.SourceSystemReference == other.SourceSystemReference ||
                    this.SourceSystemReference != null &&
                    this.SourceSystemReference.Equals(other.SourceSystemReference)
                ) &&
                (
                    this.Channel == other.Channel ||
                    this.Channel != null &&
                    this.Channel.Equals(other.Channel)
                ) &&
                (
                    this.ClassificationDetails == other.ClassificationDetails ||
                    this.ClassificationDetails != null &&
                    this.ClassificationDetails.SequenceEqual(other.ClassificationDetails)
                ) &&
                (
                    this.RecipientDetails == other.RecipientDetails ||
                    this.RecipientDetails != null &&
                    this.RecipientDetails.SequenceEqual(other.RecipientDetails)
                ) &&
                (
                    this.CustomerDetail == other.CustomerDetail ||
                    this.CustomerDetail != null &&
                    this.CustomerDetail.SequenceEqual(other.CustomerDetail)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                if (this.MessageClassification != null)
                    hash = hash * 59 + this.MessageClassification.GetHashCode();
                if (this.SourceSystemReference != null)
                    hash = hash * 59 + this.SourceSystemReference.GetHashCode();
                if (this.Channel != null)
                    hash = hash * 59 + this.Channel.GetHashCode();
                if (this.ClassificationDetails != null)
                    hash = hash * 59 + this.ClassificationDetails.GetHashCode();
                if (this.RecipientDetails != null)
                    hash = hash * 59 + this.RecipientDetails.GetHashCode();
                if (this.CustomerDetail != null)
                    hash = hash * 59 + this.CustomerDetail.GetHashCode();
                return hash;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDataForRecipientDetails(this.RecipientDetails, this.Channel))
            {
                yield return new ValidationResult("one or more error in Recipients Details", new[] { "Recipient Details" });
            }
            if (!ValidateDataForCustomerDetails(this.CustomerDetail))
            {
                yield return new ValidationResult("IdentifierType should be CIS/ID or Identifier is invalid", new[] { "Customer Detail" });
            }
        }

        private static bool ValidateDataForRecipientDetails(List<RecipientDetails> recipientDetails, string channel)
        {
            bool isValidationSuccessful = true;
            if (recipientDetails != null)
            {
                switch (channel)
                {
                    case "Email":
                        if (recipientDetails[0].ContactDetail.Count <= 10)
                        {
                            foreach (var recepientDetail in recipientDetails)
                            {
                                foreach (var recepient in recepientDetail.ContactDetail)
                                {
                                    string[] splitEmail = recepient.EmailId.Split(new char[] { ',', ';' });

                                    for (int index = 0; index < splitEmail.Length; index++)
                                    {
                                        if (!ValidateEmailId(splitEmail[index]))
                                        {
                                            isValidationSuccessful = false;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            isValidationSuccessful = false;
                        }
                        break;
                    case "SMS":
                        if (recipientDetails[0].ContactDetail.Count <= 10)
                        {
                            foreach (var recepientDetail in recipientDetails)
                            {
                                foreach (var recepient in recepientDetail.ContactDetail)
                                {
                                    if (!ValidatePhoneNumber(recepient.PhoneNumber))
                                    {
                                        isValidationSuccessful = false;
                                    }
                                }
                            }
                        }
                        else
                        {
                            isValidationSuccessful = false;
                        }
                        break;
                }
            }
            return isValidationSuccessful;
        }

        private static bool ValidateEmailId(string emailId)
        {
            return Regex.IsMatch(emailId, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }

        private static bool ValidatePhoneNumber(string phoneNumber)
        {
            return Regex.Match(phoneNumber, @"^([0]{1})([1-9]{1})([0-9]{8})$").Success;
        }

        private static bool ValidateDataForCustomerDetails(List<CustomerDetails> customerDetails)
        {
            bool isValidationSuccessful = true;
            if (customerDetails != null)
            {
                foreach (var customerDetail in customerDetails)
                {
                    if (customerDetail.IdentifierType.Equals("CIS"))
                    {
                        if (!Validateidentifier(customerDetail.Identifier))
                        {
                            isValidationSuccessful = false;
                        }
                    }
                    else if (customerDetail.IdentifierType.Equals("ID"))
                    {
                        if (!Validateidentifier(customerDetail.Identifier))
                        {
                            isValidationSuccessful = false;
                        }
                    }
                    else
                    {
                        isValidationSuccessful = false;
                    }
                }
            }
            return isValidationSuccessful;
        }

        private static bool Validateidentifier(string identifier)
        {
            if (!string.IsNullOrEmpty(identifier) && identifier.Length > 0 && identifier.Length <= 15 && Regex.IsMatch(identifier, "^[0-9]*$"))
                return true;
            return false;
        }
    }
}
