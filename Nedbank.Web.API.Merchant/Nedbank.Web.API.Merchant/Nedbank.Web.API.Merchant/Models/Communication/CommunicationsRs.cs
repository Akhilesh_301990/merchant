using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace API.BO.Communications
{
    /// <summary>
    /// CommunicationsRs
    /// </summary>
    [DataContract]
    public partial class CommunicationsRs :  IEquatable<CommunicationsRs>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationsRs" /> class.
        /// </summary>
        /// <param name="ReturnCode">response status.</param>
        /// <param name="ResultMessage">message responses.</param>
        /// <param name="ReferenceCode">reference number of the requested service..</param>
        /// <param name="SourceSystemReference">system ref. number for identification of the service.</param>
        public CommunicationsRs(string ReturnCode = default(string), string ResultMessage = default(string), string ReferenceCode = default(string), string SourceSystemReference = default(string))
        {
            this.ReturnCode = ReturnCode;
            this.ResultMessage = ResultMessage;
            this.ReferenceCode = ReferenceCode;
            this.SourceSystemReference = SourceSystemReference;
        }
        
        /// <summary>
        /// response status
        /// </summary>
        /// <value>response status</value>
        [DataMember(Name="returnCode", EmitDefaultValue=false)]
        public string ReturnCode { get; set; }
        /// <summary>
        /// message responses
        /// </summary>
        /// <value>message responses</value>
        [DataMember(Name="resultMessage", EmitDefaultValue=false)]
        public string ResultMessage { get; set; }
        /// <summary>
        /// reference number of the requested service.
        /// </summary>
        /// <value>reference number of the requested service.</value>
        [DataMember(Name="referenceCode", EmitDefaultValue=false)]
        public string ReferenceCode { get; set; }
        /// <summary>
        /// system ref. number for identification of the service
        /// </summary>
        /// <value>system ref. number for identification of the service</value>
        [DataMember(Name="sourceSystemReference", EmitDefaultValue=false)]
        public string SourceSystemReference { get; set; }
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class CommunicationsRs {\n");
            sb.Append("  ReturnCode: ").Append(ReturnCode).Append("\n");
            sb.Append("  ResultMessage: ").Append(ResultMessage).Append("\n");
            sb.Append("  ReferenceCode: ").Append(ReferenceCode).Append("\n");
            sb.Append("  SourceSystemReference: ").Append(SourceSystemReference).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            return this.Equals(obj as CommunicationsRs);
        }

        /// <summary>
        /// Returns true if CommunicationsRs instances are equal
        /// </summary>
        /// <param name="other">Instance of CommunicationsRs to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(CommunicationsRs other)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            if (other == null)
                return false;

            return 
                (
                    this.ReturnCode == other.ReturnCode ||
                    this.ReturnCode != null &&
                    this.ReturnCode.Equals(other.ReturnCode)
                ) && 
                (
                    this.ResultMessage == other.ResultMessage ||
                    this.ResultMessage != null &&
                    this.ResultMessage.Equals(other.ResultMessage)
                ) && 
                (
                    this.ReferenceCode == other.ReferenceCode ||
                    this.ReferenceCode != null &&
                    this.ReferenceCode.Equals(other.ReferenceCode)
                ) && 
                (
                    this.SourceSystemReference == other.SourceSystemReference ||
                    this.SourceSystemReference != null &&
                    this.SourceSystemReference.Equals(other.SourceSystemReference)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                if (this.ReturnCode != null)
                    hash = hash * 59 + this.ReturnCode.GetHashCode();
                if (this.ResultMessage != null)
                    hash = hash * 59 + this.ResultMessage.GetHashCode();
                if (this.ReferenceCode != null)
                    hash = hash * 59 + this.ReferenceCode.GetHashCode();
                if (this.SourceSystemReference != null)
                    hash = hash * 59 + this.SourceSystemReference.GetHashCode();
                return hash;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        { 
            yield break;
        }
    }

}
