﻿using System;
using System.Configuration;

namespace Nedbank.Web.API.DMO.Models.Providers
{
    public sealed class ConfigurationProvider
    {
        private static readonly Lazy<ConfigurationProvider> lazy =
        new Lazy<ConfigurationProvider>(() => new ConfigurationProvider());

        public static ConfigurationProvider Instance { get { return lazy.Value; } }
        private ConfigurationProvider()
        {
        }

        public string sqlConnectionString { get; set; }

        public void Init()
        {
            sqlConnectionString = ConfigurationManager.ConnectionStrings["MerchantAppConnectionString"].ToString();
        }
    }
}