﻿using Nedbank.API.Documents;
using Nedbank.Web.API.DMO.Models.DBCommunication;
using Nedbank.Web.API.Merchant.DTO.Application;
using Nedbank.Web.API.Merchant.Models;
using System;

namespace Nedbank.Web.API.DMO.Models.ProductCart
{
    public class ProductCartWrapper
    {
        /// <summary>
        /// This function faciliate the insertion of the product cart into the database table
        /// </summary>
        /// <param name="productCartInfo"></param>
        /// <returns></returns>
        public APIResponse<Int64> AddProductIntoCart(ProductCartInfo productCartInfo)
        {
            var response = new APIResponse<Int64>();
            ResultDetails resultDetails = new ResultDetails();
            ProductCartDBManager productCartDBManager = new ProductCartDBManager();
            response.Data = productCartDBManager.AddProductIntoCart(productCartInfo, out resultDetails);
            response.MetaData.ResultData = MerchantCommon.InitResultSet(resultDetails);
            return response;
        }
    }
}