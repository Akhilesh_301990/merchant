﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace Nedbank.Web.API.Merchant
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //var cors = new EnableCorsAttribute(ConfigurationManager.AppSettings["cors:allowOrigins"], "*", "*");
            //config.EnableCors(cors);

            //// Web API routes
            //config.MapHttpAttributeRoutes();

            //config.Filters.Add(new ExceptionHandlerFilterAttribute());
            //config.Filters.Add(new RequestValidationFilterAttribute());

            //////An error occurred while deserializing the CachedData property of class Nedbank.API.Caching.SessionCache: An error occurred while deserializing the Request property of class Nedbank.API.Caching.Cache: Unknown discriminator value 'XYZ'.'
            //////Register Class
            //BsonRegister.Register();
            EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }
    }
}
