﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Nedbank.Web.API.Merchant.Libraries
{
    public sealed class DBUtility : IDisposable
    {
        #region static members
        private static string _companyName;
        private static SqlConnection _sqlConnection = null;
        private static SqlTransaction _sqlTransaction = null;
        private static bool _isBeginTransactionEnabled = false;
        #endregion

        #region Implementation of Singleton Pattern
        /// <summary>
        /// This is a singleton Pattern used for DBUtility
        /// </summary>
        private static DBUtility _instance;

        /// <summary>
        /// Constructor of DBUtility class
        /// </summary>
        private DBUtility()
        {
            // TODO: Add constructor logic here        
        }

        /// <summary>
        /// Destructors of DBUtility class
        /// </summary>
        ~DBUtility()
        {
            Dispose();
        }

        /// <summary>
        /// Creating a instance of a DBUtility class
        /// </summary>
        /// <returns>returning the instance of an object</returns>
        public static DBUtility Instance(string CompanyName)
        {
            if (_instance == null)
            {
                _instance = new DBUtility();
            }
            _companyName = CompanyName;
            return _instance;
        }
        #endregion

        #region Database Activities

        /// <summary>
        /// This method is used execute all type of queries SELECT/ UPDATE/ DELETE/ INSERT
        /// </summary>
        /// <param name="cmdText">Type of Operation SELECT/ UPDATE/ DELETE/ INSERT</param>
        /// <param name="CommandTypeMethod">Type of Operation StoredProcedure = 1, TableDirect = 2, Text(i.e. SQL queries) = 3</param>
        /// <param name="CommandType">ExecuteNonQuery = 1 (Note: "@Result" parameter is compulsory required as the Output), ExecuteReader = 2, DataSet = 3, DataTable = 4 ,ExecuteScalar = 5 (Note: "@Result" parameter is compulsory required as the Output)</param>
        /// <param name="IsWithTransaction">1 for WithTransaction and 0 for WithoutTransaction</param>
        /// <param name="IsWithTransactionCompleted">True when 'IsWithTransaction' is 1 and need to compelete the same.</param>
        /// <param name="Params">If cmdText is StoredProcedure then it is required else send blank 'new System.Collections.Hashtable()' (Note: If you want to get the Output parameter then pass the parameter with name "@Result")</param>
        /// <returns>Returns the Object ExecuteNonQuery or ExecuteReader or DataSet or DataTable</returns>
        public object ExecuteQueries(string cmdText, int CommandTypeMethod, int CommandType, int IsWithTransaction, bool IsWithTransactionCompleted, Hashtable Params)
        {
            try
            {
                if (IsWithTransaction > 1)
                    throw new Exception("Invalid parameter passed for 'IsWithTransaction'");

                return _ExecuteQueries(cmdText, CommandTypeMethod, CommandType, IsWithTransaction, IsWithTransactionCompleted, Params);
            }
            catch (Exception ex)
            {
                if (_sqlConnection != null)
                {
                    if (_sqlConnection.State == ConnectionState.Open)
                    {
                        _sqlConnection.Close();
                        _sqlConnection.Dispose();
                    }
                }
                throw ex;
            }
            finally
            {
                SqlConnection.ClearAllPools();
            }
        }

        /// <summary>
        /// This method is use to excute all the type of Queries.
        /// </summary>
        /// <param name="cmdText"></param>
        /// <param name="CommandTypeMethod"></param>
        /// <param name="CommandType"></param>
        /// <param name="IsWithTransaction"></param>
        /// <param name="IsWithTransactionCompleted"></param>
        /// <param name="Params"></param>
        /// <returns></returns>
        private object _ExecuteQueries(string cmdText, int CommandTypeMethod, int CommandType, int IsWithTransaction, bool IsWithTransactionCompleted, Hashtable Params)
        {
            SqlCommand _sqlCommand = null;
            DataSet _dataSet = null;
            DataTable _dataTable = null;
            object ReturnData = null;


            if (_sqlConnection == null)
            {
                BindDBConnection();
            }
            else if (_sqlConnection.DataSource == string.Empty)
            {
                BindDBConnection();
            }
            else if (_sqlConnection != null)
            {
                if (!_sqlConnection.Database.Equals(_companyName))
                {
                    BindDBConnection();
                }
            }
            else if (IsWithTransaction.Equals(0))
            {
                _sqlConnection.Open();
            }

            //To create the process in one transaction
            if (IsWithTransaction.Equals(1) && _isBeginTransactionEnabled.Equals(false))
            {
                _isBeginTransactionEnabled = true;
                if (_sqlConnection.State == ConnectionState.Closed)
                    _sqlConnection.Open();
                _sqlTransaction = _sqlConnection.BeginTransaction();
            }

            using (_sqlCommand = new SqlCommand(cmdText, _sqlConnection))
            {
                if (_isBeginTransactionEnabled)
                {
                    _sqlCommand.Transaction = _sqlTransaction;
                }

                switch (CommandTypeMethod)
                {
                    case 1:
                        _sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        break;

                    case 2:
                        _sqlCommand.CommandType = System.Data.CommandType.TableDirect;
                        break;

                    case 3:
                        _sqlCommand.CommandType = System.Data.CommandType.Text;
                        break;
                }

                _sqlCommand.CommandTimeout = 7200;

                PrepareParams(ref _sqlCommand, Params);
                switch (CommandType)
                {
                    //ExecuteNonQuery = 1
                    case 1:

                        _sqlCommand.ExecuteNonQuery().Equals(1);
                        if (!_sqlCommand.Parameters["@Result"].Value.ToString().Equals(string.Empty))
                            ReturnData = Convert.ToBoolean(_sqlCommand.Parameters["@Result"].Value);
                        break;

                    //ExecuteReader = 2
                    case 2:
                        ReturnData = _sqlCommand.ExecuteReader();
                        break;

                    //DataSet = 3
                    case 3:
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(_sqlCommand))
                        {
                            _dataSet = new DataSet("DS");
                            sqlDataAdapter.Fill(_dataSet);
                            ReturnData = _dataSet;
                            _dataSet.Dispose();
                        }
                        break;

                    //DataTable = 4
                    case 4:
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(_sqlCommand))
                        {
                            _dataTable = new DataTable("DT");
                            sqlDataAdapter.Fill(_dataTable);
                            ReturnData = _dataTable;
                            _dataTable.Dispose();
                        }
                        break;

                    //ExecuteScalar = 5
                    case 5:
                        _sqlCommand.ExecuteScalar();
                        ReturnData = _sqlCommand.Parameters["@Result"].Value;
                        break;
                }
            }

            if (IsWithTransaction.Equals(0))
            {
                _sqlConnection.Close();
                _sqlConnection.Dispose();
            }

            if (IsWithTransactionCompleted)
            {
                _sqlTransaction.Commit();
                _sqlConnection.Close();
                _sqlConnection.Dispose();
                _isBeginTransactionEnabled = false;
            }

            return ReturnData;
        }

        /// <summary>
        /// This method is used to bind the database connection to an _sqlConnection object.
        /// </summary>
        private void BindDBConnection()
        {
            _sqlConnection = new SqlConnection(GetConnectionString());
            _sqlConnection.Open();
        }

        /// <summary>
        /// This method is used to PrepareParams on the provided parameters.
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <param name="Params"></param>
        private void PrepareParams(ref SqlCommand sqlCommand, Hashtable Params)
        {
            foreach (var _param in Params.Keys)
            {
                if (_param.Equals("@Result"))
                {
                    sqlCommand.Parameters.Add("@Result", SqlDbType.Int);
                    sqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                }
                else
                {
                    sqlCommand.Parameters.AddWithValue(_param.ToString(), Params[_param]);
                }
            }
        }

        /// <summary>
        /// This method is used to Fetch the ConnecitonString from Web.Config file.
        /// </summary>
        /// <returns>Retuns the ConnectionString in string data type</returns>
        /// <returns>Retuns the ConnectionString in string data type</returns>
        private string GetConnectionString()
        {
            return string.Format(Convert.ToString(ConfigurationManager.ConnectionStrings["WebConnecitonString"]), _companyName);
        }
        #endregion

        #region Implementing IDisposible
        /// <summary>
        /// Method to Dispose the class
        /// </summary>        
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
