/*
	Created By		: Akhilesh Kamate
	Created On		: 4/May/2018
	Discription		: This script is used to save data in MerchantLeadBusinessDetails table
*/

IF EXISTS (SELECT 1 FROM SYS.PROCEDURES WHERE NAME = 'SaveBusinessDetails')
	DROP PROCEDURE SaveBusinessDetails
GO

CREATE PROCEDURE SaveBusinessDetails
	@BusinessName					NVARCHAR(500) NULL,
	@Industry						INT NULL,
	@IndustrySubCategoryType		INT NULL,
	@BusinessLocation				INT NULL,
	@AlternateContact				VARCHAR(15) NULL,
	@MonthlyTurnover				INT NULL,
	@Result							INT OUT
AS   
	SET NOCOUNT ON;
		
		INSERT INTO MerchantLeadBusinessDetails (BusinessName,Industry,IndustrySubCategoryType,BusinessLocation, AlternateContact, MonthlyTurnover, CreatedOn, UpdatedOn)
		VALUES
		(@BusinessName, @Industry, @IndustrySubCategoryType, @BusinessLocation, @AlternateContact, @MonthlyTurnover, GETDATE(), GETDATE())

		SET @Result = 1

	SET NOCOUNT OFF;
GO 