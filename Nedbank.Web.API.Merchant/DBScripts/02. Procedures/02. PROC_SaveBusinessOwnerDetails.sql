/*
	Created By		: Akhilesh Kamate
	Created On		: 7/May/2018
	Discription		: This script is used to save data in MerchantLeadBusinessDetails table
*/

IF EXISTS (SELECT 1 FROM SYS.PROCEDURES WHERE NAME = 'PROC_SaveBusinessOwnerDetails')
	DROP PROCEDURE PROC_SaveBusinessOwnerDetails
GO

CREATE PROCEDURE PROC_SaveBusinessOwnerDetails
	@title						NVARCHAR(100),
    @firstName					NVARCHAR(100) = NULL,
    @surName					NVARCHAR(100) = NULL,
    @primaryContactNumber		NVARCHAR(100) = NULL,
	@alternateContactNumber		NVARCHAR(100) = NULL,
    @email						NVARCHAR(100) = NULL,
	@Result						INT OUT
AS   
	SET NOCOUNT ON;
		
		INSERT INTO MerchantLeadDetails (Title, Firstname, Surname, EmailAddress, PrimaryContactNumber, AlternateContactNumber, CreatedOn, UpdatedOn)
		VALUES
		(@title, @firstName, @surName, @email, @primaryContactNumber, @alternateContactNumber, GETDATE(), GETDATE())

		SET @Result = 1

	SET NOCOUNT OFF;
GO 