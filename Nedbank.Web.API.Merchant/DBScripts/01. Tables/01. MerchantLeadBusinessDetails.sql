/*
	Created By		: Ganesh Gawande/Akhilesh Kamate
	Created On		: 4/May/2018
	Discription		: This script is used to create table MerchantLeadBusinessDetails to maintain Merchant Business Details
*/
IF NOT EXISTS (SELECT 1 FROM SYS.TABLES WHERE NAME = 'MerchantLeadBusinessDetails')
BEGIN
	CREATE TABLE dbo.MerchantLeadBusinessDetails
	(
		MerchantLeadBusinessDetailsId	INT IDENTITY(1,1) NOT NULL,
		BusinessName					NVARCHAR(500) NOT NULL,
		Industry						INT NOT NULL,
		IndustrySubCategoryType			INT NOT NULL,
		BusinessLocation				INT NOT NULL,
		AlternateContact				VARCHAR(15) NULL,
		MonthlyTurnover					INT NOT NULL,
		CreatedOn						DATETIME NOT NULL,
		UpdatedOn						DATETIME NOT NULL,
	
		CONSTRAINT PK_MerchantLeadBusinessDetails PRIMARY KEY CLUSTERED 
		(
			MerchantLeadBusinessDetailsId ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO