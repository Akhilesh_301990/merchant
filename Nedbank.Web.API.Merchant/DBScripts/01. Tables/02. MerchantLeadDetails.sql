/*
	Created By		: Ganesh Gawande/Akhilesh Kamate
	Created On		: 7/May/2018
	Discription		: This script is used to create table MerchantLeadDetails to maintain Merchant Business Owner Details
*/
IF NOT EXISTS (SELECT 1 FROM SYS.TABLES WHERE NAME = 'MerchantLeadDetails')
BEGIN
	CREATE TABLE dbo.MerchantLeadDetails
	(
		MerchantLeadId			INT IDENTITY (1,1) NOT NULL,
		Title					NVARCHAR(100) NULL,
		Firstname				NVARCHAR(100) NULL,
		Surname					NVARCHAR(100) NULL,
		EmailAddress			NVARCHAR(100) NULL,
		PrimaryContactNumber	NVARCHAR(100) NULL,
		AlternateContactNumber	NVARCHAR(100) NULL,
		CreatedOn				DATETIME,
		UpdatedOn				DATETIME,
		CONSTRAINT PK_MerchantLeadDetails PRIMARY KEY CLUSTERED 
		(
			MerchantLeadId ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END
GO