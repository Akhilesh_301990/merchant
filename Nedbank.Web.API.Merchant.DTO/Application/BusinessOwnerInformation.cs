﻿using System;

namespace Nedbank.Web.API.Merchant.DTO.Application
{
    public class BusinessOwnerInformation
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public bool IsRSAResident { get; set; }
        public int IdentificationNumber { get; set; }
        public string NonRSAResident { get; set; }
        public string PassportNumber { get; set; }
        public int CountryOfIssue { get; set; }
        public DateTime PassportExpiryDate { get; set; }
        public string BusinessPermit { get; set; }
        public DateTime BusinessPermitExpiryDate { get; set; }
        public string AsylumSeeker { get; set; }
        public string asyPassportNumber { get; set; }
        public string asyCountryOfIssue { get; set; }
        public DateTime asyPassportExpiryDdate { get; set; }
        public string asyPermitNumber { get; set; }
        public DateTime asyPermitExpiryDate { get; set; }
        public string Refugee { get; set; }
        public string refPassportNumber { get; set; }
        public string refCountryOfIssue { get; set; }
        public DateTime refPassportExpiryDate { get; set; }
        public int refCertificateNumber { get; set; }
        public DateTime refCertificateExpiryDate { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CountryOfBirth { get; set; }
        public string yesPrimaryNationality { get; set; }
        public string yesAdditionalNationality1 { get; set; }
        public string yesAdditionalNationality2 { get; set; }
        public string noPrimaryNationality { get; set; }
        public bool IsForeignTaxObligations { get; set; }
        public int CellphoneNumber { get; set; }
        public int HomeTelephoneNumber { get; set; }
        public string EmailAddress { get; set; }
        //public string ConfirmEmailAddress { get; set; }
        public string pmcContact { get; set; }
        public string pmcHomeTelephone { get; set; }
        public string pmcEmail { get; set; }
        public string residentialAddress { get; set; }
        public int residentialPostalCode { get; set; }
        public string postalAddress { get; set; }
        public int postalPostalCode { get; set; }
    }
}
